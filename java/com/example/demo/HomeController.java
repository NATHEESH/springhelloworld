package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {
    @RequestMapping("/")
    public String viewHome(@RequestParam  String name, Model model)
    {
        model.addAttribute("nameisthetag",name);
       // model.addAttribute("nameisthetagtwo",name);
       return "index";
    }
}
